const {createApp} = Vue;

createApp({
    data(){
        return{
            username: '',
            password: '',
            error: null,
            correto: null,

            //arrays para armazenameto dos usuarios e senhas
            usuarios: ['admin', 'gabriel'],
            senhas: ['1234', '1234'],

            //variaveis para armanezamento do usuario e senha que sera cadastrado
            newUsername: '',
            newPassword: '',
            confirmPassword: '',

        }//Fechamento return
    },//Fechamento data

    methods:{
        login(){
            setTimeout(() => {
                if((this.username === 'Gabriel' && this.password === '1234567') || (this.username === 'Jorge' && this.password === '7654321')){
                    alert("Login Realizado com Sucesso!");
                    this.correto = "Seu login foi realizado!";
                    this.error = null;
                } //Fim do if
                else{
                    this.error = "Nome ou senha incorretos!";
                    this.correto = null;
                } //Fim do else
            }, 1000);
        },//Fechamento login

        adicionarUsuario(){
            if(!this.usuarios.includes(this.username) && this.username !== ''){
                if(this.newPassword && this.newPassword === this.confirmPassword){
                    this.usuarios.push(this.username);
                    this.senhas.push(this.newPassword);
                    this.username = '';
                    this.newPassword = '';
                    this.confirmPassword = '';

                    this.correto = 'Usuário cadastrado com sucesso!';
                    this.error = null;
                }//Fechamento if
                else{
                    this.error = 'Por favor, informe um usuário válido!';
                    this.correto = null;
            }//Fechamento else
        }//Fechamento if
        },//Fechamento adicionarUsuario
    },//Fechamento methods

}).mount("#app"); //Fechamento createApp