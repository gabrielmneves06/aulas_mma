const {createApp} = Vue;
createApp({
    data(){
        return{
            testeSpan: false,
            isLampadaLigada: false,
        }//Fim return
    },//Fim data

    methods:{
        handleTest: function()
        {
            this.testeSpan = !this.testeSpan;
        },//Fim handleTest

        toggieLampada: function()
        {
            this.isLampadaLigada = !this.isLampadaLigada;
        },//Fim toggieLampada

    },//Fim methods

}).mount("#app");