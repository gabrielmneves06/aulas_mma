const {createApp} = Vue;
createApp({
    data(){
        return {
            randomIndexInternet: 0,
            randomIndex: 0,
        
            //vetor de imagens locais
            imagensLocais:[
                './imagens/lua.jpg',
                './imagens/SENAI_logo.png',
                './imagens/sol.jpg'
            ],

            imagensInternet:[
                'https://img.freepik.com/vetores-gratis/bela-casa_24877-50819.jpg',
                'https://www.lance.com.br/files/article_main/uploads/2017/12/05/5a26b7323e0f7.jpeg',
                'https://vasco.com.br/wp-content/uploads/2021/09/ESCUDO-VASCO-RGB.png'
            ],
        }//Fim do return
    },//Fim data

    computed:{
        randomImage()
        {
            return this.imagensLocais[this.randomIndex];
        },//Fim randomImage

        randomImageInternet()
        {
            return this.imagensInternet[this.randomIndexInternet];
        }//Fim randomInternet
    },//Fim computed

    methods:{
        getRandomImage()
        {
            this.randomIndex=Math.floor(Math.random()*this.imagensLocais.length);

            this.randomIndexInternet=Math.floor(Math.random()*this.imagensInternet.length);
        }
    },//Fim methods

}).mount("#app");